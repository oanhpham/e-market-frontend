module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);
    // load npm tasks
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    // config
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      cssmin: {
        target: {
          files: [{
            'static/css/styles.min.css': ['src/assets/css/**/*.css'],
            'static/css/style.min.css': 'src/assets/css/style.css',
          }, {
            expand: true,
            cwd: 'src/components/editor/tinymce/skins',
            src: ['**/*.{css,eot,svg,woff,ttf,gif}'],
            dest: 'static/js/skins'
          }]
        }
      },
      imagemin: {
        dynamic: {
          files: [{
            expand: true,
            cwd: 'src/assets/img',
            src: ['**/*.{png,jpg,gif,ico}'],
            dest: 'static/img'
          }, {
            expand: true,
            cwd: 'src/assets/images',
            src: ['**/*.{png,jpg,gif,ico}'],
            dest: 'static/images'
          }]
        }
      },
      uglify: {
        target: {
          files: {
            'static/plugins/jquery/jquery.min.js': 'src/assets/plugins/jquery/jquery.js',
            'static/plugins/bootstrap/js/bootstrap.min.js': 'src/assets/plugins/bootstrap/js/bootstrap.js',
            'static/plugins/bootstrap-select/js/bootstrap-select.min.js': 'src/assets/plugins/bootstrap-select/js/bootstrap-select.js',
            'static/plugins/node-waves/waves.min.js': 'src/assets/plugins/node-waves/waves.js',
            'static/plugins/jquery-countto/jquery.countTo.min.js': 'src/assets/plugins/jquery-countto/jquery.countTo.js',
            'static/plugins/file-saver/FileSaver.min.js': 'src/assets/plugins/file-saver/FileSaver.js',
            'static/plugins/vue/vue-2.5.16.min.js': 'src/assets/plugins/vue/vue-2.5.16.js',
            'static/plugins/axios/axios.min.js': 'src/assets/plugins/axios/axios.js',
            'static/plugins/nprogress/nprogress.min.js': 'src/assets/plugins/nprogress/nprogress.js',
            'static/js/admin.min.js': 'src/assets/js/admin.js',
            'static/js/jquery.event.move.min.js': 'src/assets/js/jquery.event.move.js',
            'static/js/jquery.twentytwenty.min.js': 'src/assets/js/jquery.twentytwenty.js',
            'static/plugins/swiper/swiper.min.js': 'src/assets/plugins/swiper/swiper.js',
            'static/plugins/tiny_color_picker/tiny_color_picker_1.1.1.min.js': 'src/assets/plugins/tiny_color_picker/tiny_color_picker_1.1.1.js',
            'static/plugins/vue_lazyload/vue_lazyload_1.3.3.min.js': 'src/assets/plugins/vue_lazyload/vue_lazyload_1.3.3.js',
            'static/plugins/fuse/fuse_3.4.5.min.js': 'src/assets/plugins/fuse/fuse_3.4.5.js',
          }
        }
      },
      htmlmin: {
        dist: {
          options: {
            removeComments: true,
            collapseWhitespace: true
          }
        }
      },
      watch: {
        options: {
            livereload: true
        },
        css: {
          files: ['src/assets/css/*.css', 'src/assets/css/!*.min.css'],
          tasks: ['cssmin'],
          options: {
            spawn: false,
          }
        },
        img: {
          files: ['src/assets/img/**/*.{png,jpg,gif,ico}'],
          tasks: ['imagemin'],
          options: {
            spawn: false,
          }
        }
      },
    });

    function writeReleaseVersion() {
      let date = new Date()
      let releaseDate = date.getFullYear() + "" + (date.getMonth() + 1) + "" + date.getDate() + "" + date.getHours() + "" + date.getMinutes() + "" + date.getSeconds();
      let content = 'let RELEASE_VERSION = "' + releaseDate + '"';
      grunt.file.write('static/js/version.js', content);
    }
    grunt.registerMultiTask('releaseVersion', '', writeReleaseVersion());

    // register tasks
    grunt.registerTask('default', [
        'cssmin',
        'imagemin',
        'uglify',
        'htmlmin'
    ]);
}

import axios from 'axios/index'
import LoggedUser from './components/account/logged-user'

export const auth = function () {
  return axios.create({
    headers: {
      Authorization: LoggedUser.getLoggedUser().token
    }
  })
}

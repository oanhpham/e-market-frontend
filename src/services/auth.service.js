import axios from 'axios'
import * as CONST from '../constant'
const API_URL = CONST.API_URL
const ACCESS_TOKEN = CONST.ACCESS_TOKEN
const request = (options) => {
  const headers = new Headers({
    'Content-Type': 'application/json'
  })

  if (localStorage.getItem(ACCESS_TOKEN)) {
    headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
  }

  const defaults = {headers: headers}
  options = Object.assign({}, defaults, options)

  return fetch(options.url, options)
    .then(response =>
      response.json().then(json => {
        if (!response.ok) {
          return Promise.reject(json)
        }
        return json
      })
    )
}
class AuthService {
  login (user) {
    return axios
      .post(API_URL + '/auth/login', {
        username: user.username,
        password: user.password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data))
        }
        return response.data
      })
  }

  logout () {
    localStorage.removeItem('user')
  }
  getCurrentUser () {
    if (localStorage.getItem(ACCESS_TOKEN)) {
      return request({
        url: API_URL + '/user/me',
        method: 'GET'
      })
    }
  }
}

export default new AuthService()

import axios from 'axios'
import authHeader from './auth-header'
import * as CONST from '../constant'

const API_URL = CONST.API_URL

class UserService {
  getPublicContent () {
    return axios.get(API_URL + 'all')
  }
  getAdminBoard () {
    return axios.get(API_URL + 'admin', { headers: authHeader() })
  }
}

export default new UserService()

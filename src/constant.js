export const EMAIL_PATTERN = /^([a-zA-Z0-9_\-.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/
export const PHONE_PATTERN = /(^(\+?-? *[0-9]+)([,0-9 ]*)([0-9 ])*$)|(^ *$)/

export const API_URL = 'http://localhost:8080'
export const OAUTH2_REDIRECT_URI = 'http://localhost:8081/oauth2/redirect'

export const GOOGLE_AUTH_URL = API_URL + '/oauth2/authorize/google?redirect_uri=' + OAUTH2_REDIRECT_URI
export const FACEBOOK_AUTH_URL = API_URL + '/oauth2/authorize/facebook?redirect_uri=' + OAUTH2_REDIRECT_URI
export const GITHUB_AUTH_URL = API_URL + '/oauth2/authorize/github?redirect_uri=' + OAUTH2_REDIRECT_URI

export const ACCESS_TOKEN = 'accessToken'

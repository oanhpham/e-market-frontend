import Vue from 'vue'
import VueLocalStorage from 'vue-localstorage'
import * as CONST from '../../constant'
Vue.use(VueLocalStorage)

export default {
  data () {
    return {
      ACCESS_TOKEN: CONST.ACCESS_TOKEN,
      ROLE: {
        ADMIN: 0,
        USER: 1
      }
    }
  },
  methods: {
    getUrlParameter (name) {
      name = name.replace(/[[]/, '\\[').replace(/[\]]/, '\\]')
      let regex = new RegExp('[\\?&]' + name + '=([^&#]*)')

      let results = regex.exec(this.props.location.search)
      return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '))
    },
    render () {
      const token = this.getUrlParameter('token')

      if (token) {
        localStorage.setItem(this.ACCESS_TOKEN, token)
        return true
      } else {
        return false
      }
    },
    getLoggedUser () {
      const defaultUserInfo = {id: '', role: '', name: '', token: ''}
      if (Vue.localStorage.get('loggedUser')) return JSON.parse(Vue.localStorage.get('loggedUser'))
      else return defaultUserInfo
    },
    setLoggedUser (data) {
      Vue.localStorage.set('loggedUser', JSON.stringify(data))
      Vue.localStorage.setItem(CONST.ACCESS_TOKEN, this.getUrlParameter('token'))
    },
    removeLoggedUser () {
      Vue.localStorage.remove('loggedUser')
      Vue.localStorage.remove(this.ACCESS_TOKEN)
    },
    isAdmin () {
      // eslint-disable-next-line no-return-assign
      return this.getLoggedUser().role.id = this.ROLE.ADMIN
    },
    isUser () {
      // eslint-disable-next-line no-return-assign
      return this.getLoggedUser().role.id = this.ROLE.USER
    }
  }
}

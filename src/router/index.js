import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/account/Login.vue'
import Register from '@/components/account/Register.vue'

Vue.use(Router)

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login,
      meta: {
        reload: true
      }
    },
    {
      path: '/login',
      component: Login,
      meta: {
        reload: true
      }
    },
    {
      path: '/register',
      component: Register,
      meta: {
        reload: true
      }
    },
    {
      path: '/profile',
      name: 'profile',
      meta: {
        reload: true
      },
      // lazy-loaded
      component: () => import('@/components/account/Profile.vue')
    },
    {
      path: '/oauth2/redirect',
      name: 'social',
      meta: {
        reload: true
      },
      component: () => import('@/components/account/RedirectOauth.vue')
    }
  ]
})
